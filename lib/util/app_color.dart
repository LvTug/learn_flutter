import 'dart:ui';

class AppColor {
  static const NEUTRAL1 = Color(0xff000000);
  static const NEUTRAL4 = Color(0xFF555555);
  static const PRIMARY = Color(0xFFFF884B);
  static const NEUTRAL7 = Color(0xFFAAAAAA);
}
