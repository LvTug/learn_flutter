import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fqa/login/signup/sign_up.dart';
import 'package:fqa/util/app_image.dart';

class Splash extends StatelessWidget {
  const Splash({super.key});

  @override
  Widget build(BuildContext context) {
    Future.delayed(
        const Duration(seconds: 1),
        () => {
          Navigator.push(context, MaterialPageRoute(builder: (context) => SignUp()))
        });
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            body: Container(
          alignment: Alignment.center,
          child: Image.asset(AppImage.PATH_LOGO),
        )));
  }
}
