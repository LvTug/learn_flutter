import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fqa/util/app_color.dart';
import 'package:fqa/util/app_config.dart';
import 'package:fqa/util/app_dimen.dart';
import 'package:fqa/util/app_font.dart';
import 'package:fqa/util/app_string.dart';

class AccountWidget extends StatelessWidget {

  Function(String?) callBack = (value) => {};

  AccountWidget({super.key, required Function(String?) callback});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      decoration:  InputDecoration(
        prefixIcon: const Icon(Icons.person),
        hintText: AppString.HINT_SIGN_UP_ACCOUNT,
        hintStyle: Theme.of(context).textTheme.bodySmall?.copyWith(
            fontSize: AppDimen.DIMEN_16,
            color: AppColor.NEUTRAL7
        ),
        labelStyle: Theme.of(context).textTheme.bodySmall?.copyWith(
            fontSize: AppDimen.DIMEN_16,
            color: AppColor.NEUTRAL7
        ),
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(AppDimen.DIMEN_8)),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: AppColor.PRIMARY),
          borderRadius: BorderRadius.all(Radius.circular(AppDimen.DIMEN_8)),
        ),
      ),
      onSaved: (value){
        callBack(value);
      },
      validator: (String? value) {
        if (value == null || value.isEmpty) {
          return AppString.ERROR_EMPTY;
        }

        if (value.length < AppConfig.MIN_LENGTH_ACCOUNT || value.length > AppConfig.MAX_LENGTH_ACCOUNT) {
          return AppString.ERROR_ACCOUNT;
        }
        return null;
      },
    );
  }
}
