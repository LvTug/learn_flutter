import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

extension Navigate on Widget{
  void navigateTo(Widget newWidget, BuildContext ctx){
    Navigator.push(ctx, MaterialPageRoute(builder: (context) => newWidget));
  }
}
