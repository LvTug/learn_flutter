class AppString {
  static const String SIGN_UP_TITLE = " Đăng Ký";
  static const String HINT_SIGN_UP_ACCOUNT = 'Nhập tài khoản';
  static const String HINT_SIGN_UP_PASSWORD = "Nhập mật khẩu";
  static const String HINT_SIGN_UP_CONFIRM_PASSWORD = "Xác nhận mật khẩu";
  static const String ERROR_ACCOUNT = "Tên đăng nhập phải chứa ít nhất 6 ký tự và tối đa 50 ký tự";
  static const String ERROR_PASSWORD = "Mật khẩu phải chứa ít nhất 8 ký tự và tối đa 50 ký tự";
  static const String ERROR_CONFRIM_PASSWORD = "Mật khẩu không giống nhau";
  static const String ERROR_EMPTY = "Vui lòng nhập đủ thông tin";
  static const String TEXT_CHANGE_SIGN_UP_TO_SIGN_IN = "Hoặc đăng ký với";
  static const String HAVE_ACCOUNT = "Bạn đã có tài khoản?";
  static const String SIGN_IN_TITLE = " Đăng nhập";
}
