
import 'package:flutter/material.dart';
import 'package:fqa/splash/splash.dart';
import 'package:fqa/util/app_dimen.dart';
import 'package:fqa/util/app_style.dart';

void main() {
  runApp( MaterialApp(
    debugShowCheckedModeBanner: false,
    home: const Scaffold(
      body: Splash(),
    ),
    theme: ThemeData(
      textTheme: TextTheme(
          titleLarge: AppStyle.textBold.copyWith(fontSize: AppDimen.DIMEN_18),
          titleMedium: AppStyle.textMedium.copyWith(fontSize: AppDimen.DIMEN_18),
          titleSmall: AppStyle.textRegular.copyWith(fontSize: AppDimen.DIMEN_18),
          bodyLarge: AppStyle.textBold.copyWith(fontSize: AppDimen.DIMEN_16),
          bodyMedium: AppStyle.textMedium.copyWith(fontSize: AppDimen.DIMEN_16),
          bodySmall: AppStyle.textRegular.copyWith(fontSize: AppDimen.DIMEN_16),
          labelLarge: AppStyle.textBold.copyWith(fontSize: AppDimen.DIMEN_16),
          labelMedium: AppStyle.textMedium.copyWith(fontSize: AppDimen.DIMEN_16),
          labelSmall: AppStyle.textRegular.copyWith(fontSize: AppDimen.DIMEN_16),
      )
    ),
  ));
}
