class AppConfig{
  static const MIN_LENGTH_ACCOUNT = 6;
  static const MAX_LENGTH_ACCOUNT = 50;
  static const MIN_LENGTH_PASSWORD = 8;
  static const MAX_LENGTH_PASSWORD = 50;
}
