import 'package:flutter/material.dart';
import 'package:fqa/util/app_font.dart';

class AppStyle{
  static TextStyle textBold = const TextStyle(
    fontFamily: AppFont.FONT_SVN,
    fontWeight: FontWeight.w700,
  );

  static TextStyle textMedium = const TextStyle(
    fontFamily: AppFont.FONT_SVN,
    fontWeight: FontWeight.w500,
  );

  static TextStyle textSemiBold = const TextStyle(
    fontFamily: AppFont.FONT_SVN,
    fontWeight: FontWeight.w600,
  );

  static TextStyle textRegular = const TextStyle(
    fontFamily: AppFont.FONT_SVN,
    fontWeight: FontWeight.w400,
  );

  static TextStyle textLight = const TextStyle(
    fontFamily: AppFont.FONT_SVN,
    fontWeight: FontWeight.w100,
  );
}
