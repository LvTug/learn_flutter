class AppImage{
    static const String PATH_LOGO = 'res/images/logo_app.png';
    static const String IMAGE_LOGIN = 'res/images/img_login.png';
    static const String ICON_PERSON = 'res/images/ic_person.svg';
    static const String ICON_LOCK = 'res/images/ic_lock.svg';
    static const String ICON_GOOGLE = "res/images/icon_google.png";
    static const String ICON_FACEBOOK = "res/images/icon_facebook.png";
    static const String ICON_ZALO = "res/images/icon_zalo.png";
    static const String ICON_APPLE = "res/images/icon_apple.png";
}
