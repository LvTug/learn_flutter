import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:fqa/login/account_widget.dart';
import 'package:fqa/login/password_widget.dart';
import 'package:fqa/login/signin/sign_in.dart';
import 'package:fqa/util/app_color.dart';
import 'package:fqa/util/app_config.dart';
import 'package:fqa/util/app_dimen.dart';
import 'package:fqa/util/app_font.dart';
import 'package:fqa/util/app_image.dart';
import 'package:fqa/util/app_string.dart';
import 'package:toastification/toastification.dart';

class SignUp extends StatelessWidget {
  SignUp({super.key});
  late final formKey = GlobalKey<FormState>();
  String? password;
  String? confirmPassword;
  ToastificationItem? toast;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: SingleChildScrollView(
          child: Column(
            children: [
              AspectRatio(aspectRatio: 1.8 / 1, child: Image.asset(AppImage.IMAGE_LOGIN),),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: AppDimen.DIMEN_16, vertical: AppDimen.DIMEN_20),
                child: Column(
                  children: [
                     Text(
                      AppString.SIGN_UP_TITLE,
                      style: Theme.of(context).textTheme.titleLarge?.copyWith(fontSize: AppDimen.DIMEN_24),
                    ),
                    const SizedBox(height: AppDimen.DIMEN_20),
                    Form(
                        key: formKey,
                        child: Column(
                          children: [
                            AccountWidget(
                              callback: (value) => {

                              },
                            ),
                            const SizedBox(height: AppDimen.DIMEN_20),
                            PasswordField(
                                  AppString.HINT_SIGN_UP_PASSWORD,
                                  (value) => {
                                    password = value
                                  },
                            ),
                            const SizedBox(height: AppDimen.DIMEN_20),
                            PasswordField(
                                AppString.HINT_SIGN_UP_CONFIRM_PASSWORD,
                                (value) => {
                                  confirmPassword = value
                                },
                            ),
                          ],
                        )),
                    const SizedBox(height: AppDimen.DIMEN_20),
                    ElevatedButton(style: ElevatedButton.styleFrom(
                      backgroundColor: AppColor.PRIMARY,
                      minimumSize: const Size.fromHeight(AppDimen.DIMEN_52),
                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(AppDimen.DIMEN_12)),
                    ),
                      onPressed: () {
                        formKey.currentState?.validate();
                        if(formKey.currentState?.validate() == true){
                          formKey.currentState?.save();
                          if(confirmPassword != password){
                              messageError(AppString.ERROR_CONFRIM_PASSWORD, context);
                          }
                        }
                      },
                      child:  Text(
                        AppString.SIGN_UP_TITLE,
                        style:  Theme.of(context).textTheme.titleLarge?.copyWith(
                            fontSize: AppDimen.DIMEN_16,
                            color: Colors.white
                        ),
                      ),
                    ),
                    const SizedBox(height: AppDimen.DIMEN_20),
                     Text(
                      AppString.TEXT_CHANGE_SIGN_UP_TO_SIGN_IN,
                      style: Theme.of(context).textTheme.bodySmall?.copyWith(
                          fontSize: AppDimen.DIMEN_14,
                          color: Colors.black
                      ),
                    ),
                    const SizedBox(height: AppDimen.DIMEN_20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Image.asset(AppImage.ICON_GOOGLE),
                        Image.asset(AppImage.ICON_FACEBOOK),
                        Image.asset(AppImage.ICON_ZALO),
                        Image.asset(AppImage.ICON_APPLE),
                      ],
                    ),
                    const SizedBox(height: AppDimen.DIMEN_20),
                    RichText(
                      text: TextSpan(
                        style: Theme.of(context).textTheme.bodySmall?.copyWith(
                            fontSize: AppDimen.DIMEN_16,
                            color: Colors.black
                        ),
                        children: [
                          const TextSpan(text: AppString.HAVE_ACCOUNT),
                          TextSpan(
                            text: AppString.SIGN_UP_TITLE,
                            style:  Theme.of(context).textTheme.bodyLarge?.copyWith(
                                fontSize: AppDimen.DIMEN_14,
                                color: AppColor.PRIMARY
                            ),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => SignIn()),
                                );
                              },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        )
    );
  }

  bool showError(String account, String password, String confirm, BuildContext context) {
    var isContinue = true;

    if (account.isEmpty || password.isEmpty || confirm.isEmpty) {
      messageError(AppString.ERROR_EMPTY, context);
      isContinue = false;
      return isContinue;
    }

    if (account.length < AppConfig.MIN_LENGTH_ACCOUNT || account.length > AppConfig.MAX_LENGTH_ACCOUNT) {
      messageError(AppString.ERROR_ACCOUNT, context);
      isContinue = false;
    }

    if (password.length < AppConfig.MIN_LENGTH_PASSWORD || password.length > AppConfig.MAX_LENGTH_PASSWORD) {
      messageError(AppString.ERROR_PASSWORD, context);
      isContinue = false;
    }

    if (password != confirm) {
      messageError(AppString.ERROR_CONFRIM_PASSWORD, context);
      isContinue = false;
    }
    return isContinue;
  }

  void messageError(String message, BuildContext context) {
    toastification.show(
        context: context,
        type: ToastificationType.error,
        style: ToastificationStyle.fillColored,
        title: message,
        alignment: Alignment.topCenter,
        autoCloseDuration: const Duration(seconds: 3),
        borderRadius: BorderRadius.circular(AppDimen.DIMEN_12),
        closeButtonShowType: CloseButtonShowType.none,
        dragToClose: true,
        showProgressBar: false);
  }
}
