import 'dart:ffi';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fqa/util/app_color.dart';
import 'package:fqa/util/app_config.dart';
import 'package:fqa/util/app_dimen.dart';
import 'package:fqa/util/app_font.dart';
import 'package:fqa/util/app_string.dart';

class PasswordField extends StatefulWidget {
  String hintText;
  Function(String?) callBack;

  PasswordField(this.hintText, this.callBack,{super.key});

  @override
  State<StatefulWidget> createState() {
    return _PasswordFieldState(hintText);
  }
}

class _PasswordFieldState extends State<PasswordField> {
  String _hintText;
  bool _obscured = true;

  _PasswordFieldState(this._hintText);

  void _toggleObscured() {
    setState(() {
      _obscured = !_obscured;
    });
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      keyboardType: TextInputType.visiblePassword,
      obscureText: _obscured,
      decoration: InputDecoration(
        hintText: _hintText,
        hintStyle: Theme.of(context).textTheme.bodySmall?.copyWith(
            fontSize: AppDimen.DIMEN_16,
            color: AppColor.NEUTRAL7
        ),
        labelStyle: Theme.of(context).textTheme.bodySmall?.copyWith(
            fontSize: AppDimen.DIMEN_16,
            color: AppColor.NEUTRAL7
        ),
        border: const OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(AppDimen.DIMEN_8)),
        ),
        focusedBorder: const OutlineInputBorder(
            borderSide: BorderSide(color: AppColor.PRIMARY),
            borderRadius: BorderRadius.all(Radius.circular(AppDimen.DIMEN_8))),
        prefixIcon: const Icon(Icons.lock_rounded, size: AppDimen.DIMEN_24),
        suffixIcon: Padding(
          padding: const EdgeInsets.fromLTRB(0, 0, AppDimen.DIMEN_4, 0),
          child: GestureDetector(
            onTap: _toggleObscured,
            child: Icon(
              _obscured ? Icons.visibility_off_rounded : Icons.visibility_rounded,
              size: AppDimen.DIMEN_24,
            ),
          ),
        ),
      ),
      onSaved: (value) {
        print('co callback $value');
        widget.callBack(value);
      },
      validator: (String? value) {
        if (value == null || value.isEmpty) {
          return AppString.ERROR_EMPTY;
        }

        if (value.length < AppConfig.MIN_LENGTH_PASSWORD || value.length > AppConfig.MAX_LENGTH_PASSWORD) {
          return AppString.ERROR_PASSWORD;
        }
        return null;
      },
    );
  }
}
